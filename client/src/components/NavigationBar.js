import React, { Component } from "react";
import { Nav, Navbar, Button } from "react-bootstrap";
import { NavLink } from "react-router-dom";
import PropTypes from "prop-types";
import "./NavigationBar.css";
import { connect } from "react-redux";
import { logoutUser } from "../actions/authActions";

class NavigationBar extends Component {
  onLogoutClick = e => {
    e.preventDefault();
    this.props.logoutUser();
  };

  render() {
    const isAdmin = this.props.auth.isAdmin;
    const isLoggedIn = this.props.auth.isAuthenticated;
    return (
      <Navbar
        default
        collapsOnSelect
        expand="lg"
        fixed="top"
        className="border-bottom border-dark"
      >
        <Navbar.Brand as={NavLink} to="/">
          THE HOTEL
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse>
          <Nav className="ml-auto d-flex flex-row">
            <Nav.Item>
              <Nav.Link exact eventKey="1" as={NavLink} to="/">
                Home
              </Nav.Link>
            </Nav.Item>
            <Nav.Item>
              <Nav.Link eventKey="2" as={NavLink} to="/rooms">
                Rooms
              </Nav.Link>
            </Nav.Item>
            <Nav.Item>
              <Nav.Link eventKey="3" as={NavLink} to="/contact">
                Contact
              </Nav.Link>
            </Nav.Item>
            {isLoggedIn ? (
              <React.Fragment>
                {isAdmin ? (
                  <Nav.Item>
                    <Nav.Link eventKey="4" as={NavLink} to="/dashboard">
                      Dashboard
                    </Nav.Link>
                  </Nav.Item>
                ) : null}
                <Button
                  className="nav-button"
                  variant="secondary"
                  onClick={this.onLogoutClick}
                >
                  LOGOUT
                </Button>
              </React.Fragment>
            ) : (
              <React.Fragment>
                <Nav.Item>
                  <Nav.Link eventKey="5" as={NavLink} to="/login">
                    Login
                  </Nav.Link>
                </Nav.Item>
                <Nav.Item>
                  <Nav.Link eventKey="6" as={NavLink} to="/signup">
                    SignUp
                  </Nav.Link>
                </Nav.Item>
              </React.Fragment>
            )}
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    );
  }
}

NavigationBar.propTypes = {
  logoutUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth
});

export default connect(
  mapStateToProps,
  { logoutUser }
)(NavigationBar);
