import React, { Component } from "react";
import { Form, Button } from "react-bootstrap";
import "./signup.css";
import { withRouter } from "react-router-dom";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { signupUser } from "../actions/authActions";
import classnames from "classnames";

class Signup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      surname: "",
      email: "",
      password: "",
      errors: {}
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.errors) {
      this.setState({
        errors: nextProps.errors
      });
    }
  }

  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  onSubmit = e => {
    e.preventDefault();
    const newUser = {
      name: this.state.name,
      surname: this.state.surname,
      email: this.state.email,
      password: this.state.password
    };
    this.props.signupUser(newUser, this.props.history);
  };

  render() {
    const { errors } = this.state;
    return (
      <div className="Signup">
        <Form onSubmit={this.onSubmit}>
          <Form.Group controlId="formGridName">
            <Form.Label>Name</Form.Label>
            <Form.Control
              placeholder="Name"
              name="name"
              type="text"
              onChange={this.onChange}
              value={this.state.name}
              error={errors.name}
              className={classnames("", {
                invalid: errors.name
              })}
            />
            <Form.Text className="text-muted">{errors.name}</Form.Text>
          </Form.Group>

          <Form.Group controlId="formGridSurname">
            <Form.Label>Surname</Form.Label>
            <Form.Control
              placeholder="Surname"
              name="surname"
              type="text"
              onChange={this.onChange}
              value={this.state.surname}
              error={errors.name}
              className={classnames("", {
                invalid: errors.surname
              })}
            />
            <Form.Text className="text-muted">{errors.surname}</Form.Text>
          </Form.Group>

          <Form.Group controlId="formGridEmail">
            <Form.Label>Email</Form.Label>
            <Form.Control
              type="email"
              name="email"
              placeholder="Enter email"
              onChange={this.onChange}
              value={this.state.email}
              error={errors.name}
              className={classnames("", {
                invalid: errors.email
              })}
            />
            <Form.Text className="text-muted">{errors.email}</Form.Text>
          </Form.Group>
          <Form.Group controlId="formGridPassword">
            <Form.Label>Password</Form.Label>
            <Form.Control
              type="password"
              name="password"
              placeholder="Enter password"
              onChange={this.onChange}
              value={this.state.password}
              error={errors.name}
              className={classnames("", {
                invalid: errors.password
              })}
            />
            <Form.Text className="text-muted">{errors.password}</Form.Text>
          </Form.Group>
          <Button block type="submit" variant="secondary">
            Submit
          </Button>
        </Form>
      </div>
    );
  }
}

Signup.propTypes = {
  signupUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth,
  errors: state.errors
});

export default connect(
  mapStateToProps,
  { signupUser }
)(withRouter(Signup));
