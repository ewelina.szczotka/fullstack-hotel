import React, { Component } from "react";
import {
  Image,
  Carousel,
  Jumbotron,
  Button,
  Container,
  Row,
  Col
} from "react-bootstrap";
import "./home.css";

class Landing extends Component {
  state = {};
  render() {
    return (
      <div style={{ width: "100%", margin: "auto" }}>
        <Carousel>
          <Carousel.Item>
            <img
              src="/images/img-hotel3.jpg"
              alt="Hotel_Image"
              className="hotel-img"
            />
            <Carousel.Caption>
              <h3>AWESOME PICTURE</h3>
              <p>
                Ut enim ad minim veniam, quis nostrud exercitation ullamco
                laboris nisi ut aliquip ex ea commodo consequat.
              </p>
            </Carousel.Caption>
          </Carousel.Item>
          <Carousel.Item>
            <img
              src="/images/img-hotel2.jpg"
              alt="Hotel_Image"
              className="hotel-img"
            />
            <Carousel.Caption>
              <h3>AWESOME PICTURE</h3>
              <p>
                Duis aute irure dolor in reprehenderit in voluptate velit esse
                cillum dolore eu fugiat nulla pariatur.
              </p>
            </Carousel.Caption>
          </Carousel.Item>
          <Carousel.Item>
            <img
              src="/images/img-hotel1.jpg"
              alt="Hotel_Image"
              className="hotel-img"
            />
            <Carousel.Caption>
              <h3>AWESOME PICTURE</h3>
              <p>Excepteur sint occaecat cupidatat non proident.</p>
            </Carousel.Caption>
          </Carousel.Item>
        </Carousel>

        <Jumbotron className="text-center">
          <h1>Our rooms</h1>
          <Container>
            <Row className="show-grid text-center">
              <Col sm={12} md={3}>
                <Image
                  src="/images/single-room.jpg"
                  rounded
                  className="hotel-img"
                />
                <h2>Single Room | 40$</h2>
              </Col>
              <Col sm={12} md={3}>
                <Image
                  src="/images/double-room.jpg"
                  rounded
                  className="hotel-img"
                />
                <h2>Double Room | 80$</h2>
              </Col>
              <Col sm={12} md={3}>
                <Image
                  src="/images/apartment.jpg"
                  rounded
                  className="hotel-img"
                />
                <h2>Apartment | 100$</h2>
              </Col>
              <Col sm={12} md={3}>
                <Image src="/images/studio.jpg" rounded className="hotel-img" />
                <h2>Studio | 150$</h2>
              </Col>
            </Row>
          </Container>
          <p>
            <Button variant="secondary" href="/rooms" size="lg">
              LEARN MORE
            </Button>
          </p>
        </Jumbotron>
      </div>
    );
  }
}

export default Landing;
