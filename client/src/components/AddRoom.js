import React, { Component } from "react";
import "./AddRoom.css";
import { Form, Button, FormLabel, FormControl } from "react-bootstrap";
import classnames from "classnames";
import { addRoom } from "../actions/authActions";
import PropTypes from "prop-types";
import { connect } from "react-redux";

class AddRoom extends Component {
  constructor(props) {
    super(props);

    this.state = {
      number: "",
      beds: "",
      price: "",
      errors: {}
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.errors) {
      this.setState({
        errors: nextProps.errors
      });
    }
  }

  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  onSubmit = e => {
    e.preventDefault();
    const newRoom = {
      number: this.state.number,
      beds: this.state.beds,
      price: this.state.price
    };
    this.props.addRoom(newRoom, this.props.history);
  };

  render() {
    const { errors } = this.state;
    return (
      <div className="AddRoom">
        <Form onSubmit={this.onSubmit}>
          <FormLabel>Number:</FormLabel>
          <FormControl
            placeholder="Room's number"
            type="number"
            name="number"
            onChange={this.onChange}
            value={this.state.number}
            error={errors.number}
            className={classnames("", {
              invalid: errors.number
            })}
          />
          <Form.Text className="text-muted">{errors.number}</Form.Text>

          <FormLabel>Beds:</FormLabel>
          <FormControl
            onChange={this.onChange}
            value={this.state.beds}
            error={errors.beds}
            placeholder="Number of beds"
            type="number"
            name="beds"
            className={classnames("", {
              invalid: errors.beds
            })}
          />
          <Form.Text className="text-muted">{errors.beds}</Form.Text>

          <FormLabel>Price:</FormLabel>
          <FormControl
            onChange={this.onChange}
            value={this.state.price}
            error={errors.price}
            placeholder="Price"
            type="number"
            name="price"
            className={classnames("", {
              invalid: errors.price
            })}
          />
          <Form.Text className="text-muted">{errors.price}</Form.Text>

          <Button
            block
            className="add-button"
            variant="secondary"
            type="submit"
          >
            Add room
          </Button>
        </Form>
      </div>
    );
  }
}

AddRoom.propTypes = {
  addRoom: PropTypes.func.isRequired,
  errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  errors: state.errors
});

export default connect(
  mapStateToProps,
  { addRoom }
)(AddRoom);
