import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Table, Button } from "react-bootstrap";
import axios from "axios";
import "./GetRooms.css";

class GetRooms extends Component {
  constructor(props) {
    super(props);
    this.state = { rooms: [] };
  }

  componentDidMount() {
    axios
      .get("/rooms")
      .then(response => {
        this.setState({ rooms: response.data });
      })
      .catch(function(error) {
        console.log(error);
      });
    /** 
    this.interval = setInterval(
      () =>
        axios
          .get("/rooms")
          .then(response => {
            this.setState({ rooms: response.data });
          })
          .catch(function(error) {
            console.log(error);
          }),
      1000
    );
    */
  }

  getAllRooms() {
    this.setState({ state: this.state });
  }

  deleteRoom = (e, room) => {
    e.preventDefault();
    axios
      .delete(`/rooms/${room._id}`)
      .then(res => {
        this.setState(previousState => {
          return {
            rooms: previousState.rooms.filter(r => r._id !== room._id)
          };
        });
      })
      .catch(err => {
        console.log(err);
      });
  };

  render() {
    return (
      <div className="get-rooms">
        <Table striped bordered hover>
          <thead>
            <tr>
              <th>Number</th>
              <th>Beds</th>
              <th>Price</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            {this.state.rooms.map((room, i) => {
              return (
                <tr key={i}>
                  <td>{room.number}</td>
                  <td>{room.beds}</td>
                  <td>{"$" + room.price}</td>
                  <td>
                    <Button
                      variant="outline-danger"
                      type="submit"
                      onClick={e => this.deleteRoom(e, room)}
                    >
                      Delete
                    </Button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </Table>
      </div>
    );
  }
}

export default GetRooms;
