import React, { Component } from "react";
import { Form, Button } from "react-bootstrap";
import "./contact.css";

class Contact extends Component {
  constructor(props) {
    super(props);

    this.state = {
      name: null,
      surname: null,
      email: null,
      message: null,
      errors: {
        name: "Name must be 3 characters long.",
        surname: "Surname must be 3 characters long.",
        email: "Email is not valid.",
        message: "Message can't be empty."
      }
    };
  }

  handleUserInput = e => {
    e.preventDefault();
    const { name, value } = e.target;
    let errors = this.state.errors;

    switch (name) {
      case "name":
        errors.name = value.length < 3 ? "Name must be 3 characters long." : "";
        break;
      case "surname":
        errors.surname =
          value.length < 3 ? "Surname must be 3 characters long." : "";
        break;
      case "email":
        errors.email = value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i)
          ? ""
          : "Email is not valid.";
        break;
      case "message":
        errors.password = value.length < 1 ? "Message can't be empty." : "";
        break;
      default:
        break;
    }

    this.setState({ errors, [name]: value });
  };

  handleSubmit = event => {
    event.preventDefault();
    if (validateForm(this.state.errors)) {
      /* todo - server connection */
    } else {
      console.info("Invalid form");
    }
  };

  render() {
    const { errors } = this.state;
    return (
      <div className="Contact">
        <Form onSubmit={this.handleSubmit}>
          <Form.Group controlId="formGridName">
            <Form.Label>Name</Form.Label>
            <Form.Control
              placeholder="Name"
              name="name"
              type="text"
              isValid={errors.name.length === 0}
              value={this.state.name}
              onChange={this.handleUserInput}
            />
          </Form.Group>

          <Form.Group controlId="formGridSurname">
            <Form.Label>Surname</Form.Label>
            <Form.Control
              placeholder="Surname"
              name="surname"
              type="text"
              isValid={errors.surname.length === 0}
              value={this.state.surname}
              onChange={this.handleUserInput}
            />
          </Form.Group>

          <Form.Group controlId="formGridEmail">
            <Form.Label>Email</Form.Label>
            <Form.Control
              type="email"
              placeholder="Enter email"
              name="email"
              isValid={errors.email.length === 0}
              value={this.state.email}
              onChange={this.handleUserInput}
            />
          </Form.Group>
          <Form.Group controlId="formTextArea">
            <Form.Label>Message</Form.Label>
            <Form.Control
              as="textarea"
              rows="6"
              placeholder="Your message"
              name="message"
              isValid={errors.message.length === 0}
              value={this.state.password}
              onChange={this.handleUserInput}
            />
          </Form.Group>
          <Button block bsSize="large" type="submit" variant="secondary">
            Send
          </Button>
        </Form>
      </div>
    );
  }
}

const validateForm = errors => {
  let valid = true;
  Object.values(errors).forEach(val => val.length > 0 && (valid = false));
  return valid;
};

export default Contact;
