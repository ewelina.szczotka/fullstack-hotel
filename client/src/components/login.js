import React, { Component } from "react";
import {
  Button,
  FormGroup,
  FormControl,
  FormLabel,
  Form
} from "react-bootstrap";
import "./login.css";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { loginUser } from "../actions/authActions";
import classnames from "classnames";

class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: "",
      password: "",
      errors: {}
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.auth.isAdmin) {
      this.props.history.push("/dashboard"); // push user to dashboard when they login
    } else if (nextProps.auth.isAuthenticated) {
      this.props.history.push("/");
    }
    if (nextProps.errors) {
      this.setState({
        errors: nextProps.errors
      });
    }
  }

  onChange = e => {
    this.setState({ [e.target.id]: e.target.value });
  };

  onSubmit = e => {
    e.preventDefault();
    const userData = {
      email: this.state.email,
      password: this.state.password
    };
    this.props.loginUser(userData); // since we handle the redirect within our component, we don't need to pass in this.props.history as a parameter
  };

  render() {
    const { errors } = this.state;
    return (
      <div className="Login">
        <Form onSubmit={this.onSubmit}>
          <FormGroup controlId="email" bsSize="large">
            <FormLabel>Email:</FormLabel>
            <FormControl
              autoFocus
              placeholder="Enter email"
              type="email"
              name="email"
              onChange={this.onChange}
              value={this.state.email}
              error={errors.email}
              className={classnames("", {
                invalid: errors.email || errors.emailnotfound
              })}
            />
            <Form.Text className="text-muted">
              {errors.email}
              {errors.emailnotfound}
            </Form.Text>
          </FormGroup>
          <FormGroup controlId="password" bsSize="large">
            <FormLabel>Password:</FormLabel>
            <FormControl
              onChange={this.onChange}
              value={this.state.password}
              error={errors.password}
              placeholder="Password"
              type="password"
              name="password"
              className={classnames("", {
                invalid: errors.password || errors.passwordincorrect
              })}
            />
            <Form.Text className="text-muted">
              {" "}
              {errors.password}
              {errors.passwordincorrect}
            </Form.Text>
          </FormGroup>
          <Button block variant="secondary" bsSize="large" type="submit">
            Login
          </Button>
        </Form>
      </div>
    );
  }
}

Login.propTypes = {
  loginUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired
};
const mapStateToProps = state => ({
  auth: state.auth,
  errors: state.errors
});
export default connect(
  mapStateToProps,
  { loginUser }
)(Login);
