import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { logoutUser } from "../actions/authActions";
import AddRoom from "./AddRoom";
import { Container, Row, Col } from "react-bootstrap";
import GetRooms from "./GetRooms";
import "./dashboard.css";

class Dashboard extends Component {
  onLogoutClick = e => {
    e.preventDefault();
    this.props.logoutUser();
  };

  render() {
    const { user } = this.props.auth;
    return (
      <div className="dashboard">
        <h4>
          <b>Hello</b> {user.name.split(" ")[0]}
        </h4>
        <Container>
          <Row>
            <Col sm={12} md={6}>
              <AddRoom />
            </Col>
            <Col sm={12} md={6}>
              <GetRooms />
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

Dashboard.propTypes = {
  logoutUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth
});

export default connect(
  mapStateToProps,
  { logoutUser }
)(Dashboard);
