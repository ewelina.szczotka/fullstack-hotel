const Validator = require("validator");
const isEmpty = require("is-empty");

module.exports = function validateRoomInput(data) {
  let errors = {};

  data.number = !isEmpty(data.number) ? data.number : "";
  data.beds = !isEmpty(data.beds) ? data.beds : "";
  data.price = !isEmpty(data.price) ? data.price : "";

  if (Validator.isEmpty(data.number)) {
    errors.number = "Number field is required";
  }

  if (Validator.isEmpty(data.beds)) {
    erroradata.beds = "Beds field is required";
  }

  if (Validator.isEmpty(data.price)) {
    errors.price = "Price field is required";
  }

  return {
    errors,
    isValid: isEmpty(errors)
  };
};
