const jwt = require('jsonwebtoken');


exports.checkUser = (req,res,next) => {
    try{
        const token = req.headers.authorization.split(" ")[1];
        const decoded = jwt.verify(token, process.env.JWT_KEY);
        req.userData = decoded;
        next();
    } catch (error) {
        return res.status(401).json({
            message: 'Authorization failed.'
        });
    }
};

exports.checkAdmin = (req,res,next) => {
    try{
        const token = req.headers.authorization.split(" ")[1];
        const decoded = jwt.verify(token, process.env.JWT_KEY);
        req.userData = decoded;
        console.log(req.userData.admin);
        if(!req.userData.admin){
            return res.status(401).json({
                message: 'No permission.'
            });
        }
        next();
    } catch (error) {
        return res.status(401).json({
            message: 'Authorization failed.'
        });
    }
};