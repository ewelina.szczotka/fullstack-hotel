const Room = require("../models/room");
const mongoose = require("mongoose");
const validateRoomInput = require("../validation/rooms");

exports.rooms_get_all = (req, res, next) => {
  Room.find(function(err, rooms) {
    if (err) {
      console.log(err);
    } else {
      res.json(rooms);
    }
  });
};

exports.rooms_post = (req, res) => {
  const { errors, isValid } = validateRoomInput(req.body);
  // Check validation
  if (!isValid) {
    return res.status(400).json(errors);
  }

  Room.findOne({ number: req.body.number }).then(room => {
    if (room) {
      return res
        .status(400)
        .json({ number: "Room with this number already exists" });
    } else {
      const newRoom = new Room({
        number: req.body.number,
        beds: req.body.beds,
        price: req.body.price
      });
      newRoom
        .save()
        .then(room => res.json(room))
        .catch(err => console.log(err));
    }
  });
};

exports.rooms_get_one = (req, res, next) => {
  const id = req.params.roomId;
  Room.findById(id)
    .select("-__v")
    .exec()
    .then(doc => {
      if (doc) {
        res.status(200).json({
          room: doc,
          request: {
            type: "GET",
            description: "GET_ALL_ROOMS",
            url: "http://localhost:3000/rooms/"
          }
        });
      } else {
        res.status(404).json({ message: "No valid entry found." });
      }
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
};

exports.rooms_patch = (req, res, next) => {
  const id = req.params.roomId;
  const updateOps = {};
  for (const ops of req.body) {
    updateOps[ops.propName] = ops.value;
  }
  Room.update({ _id: id }, { $set: updateOps })
    .exec()
    .then(result => {
      res.status(200).json({
        message: "Room updated.",
        request: {
          type: "GET",
          url: "http://localhost:3000/rooms/" + id
        }
      });
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
};

exports.rooms_delete = (req, res, next) => {
  const id = req.params.roomId;
  Room.remove({ _id: id })
    .exec()
    .then(result => {
      res.status(200).json({});
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
};
