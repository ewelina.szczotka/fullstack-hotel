const mongoose = require("mongoose");

const roomSchema = mongoose.Schema({
  number: { type: Number, required: true, unique: true },
  beds: { type: Number, required: true },
  price: { type: Number, required: true }
});

module.exports = mongoose.model("Room", roomSchema);
