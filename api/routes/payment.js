const paypal = require('paypal-rest-sdk');
const express = require('express');
const router = express.Router();
const Reservation = require('../models/reservation');

paypal.configure({
    'mode': 'sandbox', 
    'client_id': 'AZ9eS40VQuZ76QpCJAE44IjQg5rfFDq--ru51QjV8Ex-L27miA-cttBAbRac9fLywPZwedRrJL5ZnKJO',
    'client_secret': 'EPnkt5NQv5hzp0413uVvAsRoLYphq2Kik41oV5YA6xcEDVJdDDofNG7SnYxCLqbPO5crMKAzjBfN_6j8'
  });

router.post('/pay', (req,res,next) => {
    
    const create_payment_json = {
        "intent": "sale",
        "payer": {
            "payment_method": "paypal"
        },
        "redirect_urls": {
            "return_url": "http://localhost:3000/success",
            "cancel_url": "http://localhost:3000/cancel"
        },
        "transactions": [{
            "item_list": {
                "items": [{
                    "name": "order",
                    "sku": "order",
                    "price": "90.00",
                    "currency": "PLN",
                    "quantity": 1
                }]
            },
            "amount": {
                "currency": "PLN",
                "total": "90.00"
            },
            "description": "Reservation"
        }]
    };
    
    paypal.payment.create(create_payment_json, (error, payment) => {
      if (error) {
        console.log(error);
        return res.status(500).json({
            message: 'Could not create payment'
        });
      } else {
          for(let i = 0;i < payment.links.length;i++){
            if(payment.links[i].rel === 'approval_url'){
              res.redirect(payment.links[i].href);
            }
          }
      }
    });
});

router.get('/success', (req, res) => {
    const payerId = req.query.PayerID;
    const paymentId = req.query.paymentId;
  
    const execute_payment_json = {
      "payer_id": payerId,
      "transactions": [{
          "amount": {
              "currency": "PLN",
              "total": "90.00"
          }
      }]
    };
  
    paypal.payment.execute(paymentId, execute_payment_json, (error, payment) => {
      if (error) {
          console.log(error.response);
          throw error;
      } else {
          console.log(JSON.stringify(payment));
          res.send('Payment successfull.');
      }
  });
  });
  
router.get('/cancel', (req, res) => res.send('Cancelled'));

module.exports = router;
