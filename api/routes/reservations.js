const express = require('express');
const router = express.Router();
const checkAuth = require('../authorization/checkAuth');

const ReservationsController = require('../controllers/reservations');


router.get('/', checkAuth.checkAdmin, ReservationsController.reservations_get_all);

router.post('/', checkAuth.checkUser, ReservationsController.reservations_post);

router.get('/:reservationId', checkAuth.checkAdmin, ReservationsController.reservations_get_one);

router.delete('/:reservationId', checkAuth.checkAdmin, ReservationsController.reservations_delete);


module.exports = router;