const express = require("express");
const router = express.Router();
const checkAuth = require("../authorization/checkAuth");

const RoomsController = require("../controllers/rooms");

router.get("/", RoomsController.rooms_get_all);

router.post("/add", RoomsController.rooms_post);

router.get("/:roomId", RoomsController.rooms_get_one);

router.patch("/:roomId", checkAuth.checkAdmin, RoomsController.rooms_patch);

router.delete("/:roomId", RoomsController.rooms_delete);

module.exports = router;
